// Importando as bibliotecas e componentes necessários do React Native
import React, { useState } from "react";
import { View, TextInput, Button, StyleSheet, Text, Modal, TouchableOpacity } from "react-native";
import { AntDesign } from "@expo/vector-icons"; 
import apiFunctions from './api'; 

// Definindo o componente LoginScreen
const LoginScreen = ({ navigation }) => {
  // Definindo estados para os campos do formulário, estado do modal e mensagens do modal
  const [Email, setEmail] = useState('');
  const [Senha, setSenha] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [modalTitle, setModalTitle] = useState('');
  const [modalMessage, setModalMessage] = useState('');

  // Função para lidar com o processo de login
  const handleLogin = async () => {
    try {
      
      if (!Email || !Senha) {
        setModalTitle('Erro');
        setModalMessage('Por favor, preencha todos os campos.');
        
        setTimeout(() => setModalVisible(true), 100);
        return;
      }

      // Realizando a solicitação de login para a API
      const response = await apiFunctions.postLogin({ Email: Email, Senha: Senha });
      
      // Verificando a resposta da API
      if (response.status === 200) {
        
        setModalTitle('Sucesso');
        setModalMessage('Login bem-sucedido.');
        
        setTimeout(() => {
          setModalVisible(true);
          navigation.navigate('Home');
        }, 300);
      } else {
        
        setModalTitle('Erro');
        setModalMessage('Credenciais inválidas');
        
        setTimeout(() => setModalVisible(true), 100);
      }
    } catch (error) {
      
      setModalTitle('Erro');
      setModalMessage('Algo deu errado. Tente novamente mais tarde.');
      
      setTimeout(() => setModalVisible(true), 100);
    }
  };

  // Renderização da interface
  return (
    <View style={styles.container}>
      {/* Modal para exibir mensagens de sucesso ou erro */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalTitle}>{modalTitle}</Text>
            <Text style={styles.modalText}>{modalMessage}</Text>
            {/* Botão no modal para fechar */}
            <Button
              onPress={() => setModalVisible(!modalVisible)}
              title="OK"
              color="#3e945a"
            />
          </View>
        </View>
      </Modal>
      {/* Título do formulário */}
      <Text style={styles.textInput}>INSIRA SUAS INFORMAÇÕES DE LOGIN</Text>
      {/* Campo de entrada para email */}
      <TextInput
        style={styles.input}
        placeholder="Email"
        onChangeText={setEmail}
        value={Email}
      />
      {/* Campo de entrada para senha */}
      <TextInput
        style={styles.input}
        placeholder="Senha"
        secureTextEntry={true}
        onChangeText={setSenha}
        value={Senha}
      />
      {/* Botão de login */}
      <View style={styles.buttonContainer}>
        <Button title="LOGIN" onPress={handleLogin} color="#3e945a" />
      </View>
      {/* Botão "Esqueceu a senha?" */}
      <TouchableOpacity onPress={() => console.log("Esqueceu a senha?")} style={styles.forgotPassword}>
        <Text style={styles.forgotPasswordText}>Esqueceu a senha?</Text>
      </TouchableOpacity>
      {/* Botão para voltar */}
      <TouchableOpacity style={styles.backButton} onPress={() => navigation.navigate("PaginaInicial")}>
        <AntDesign name="arrowleft" size={24} color="#3e945a" />
      </TouchableOpacity>
      {/* Rodapé */}
      <View style={styles.footer}>
        <Text style={styles.footerText}>© 2024 Reserva de Quadras Esportivas</Text>
      </View>
    </View>
  );
};

// Estilos para os componentes
const styles = StyleSheet.create({
  // Estilos para o contêiner principal
  container: {
    flex: 1,
    backgroundColor: '#faf2f2',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
    marginLeft: 10,
    marginRight: 10,
  },
  // Estilos para o título do formulário
  textInput: {
    height: 20,
    width: 300,
    marginBottom: 16,
    fontSize: 15,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  // Estilos para os campos de entrada
  input: {
    borderColor: '#4dbf72',
    borderWidth: 3,
    borderRadius: 30,
    fontSize: 12,
    width: '80%',
    padding: 10,
    marginVertical: 3,
    marginTop: 3,
    marginBottom: 5,
    paddingHorizontal: 10,
    height: 40,
  },
  // Estilos para o botão de login
  buttonContainer: {
    backgroundColor: '#e6e6e6',
    borderRadius: 20,
    overflow: 'hidden',
    width: '80%',
    marginBottom: 3,
    marginVertical: 15,
    marginTop: 4,
  },
  // Estilos para o rodapé
  footer: {
    position: 'absolute',
    bottom: 0,
  },
  footerText: {
    fontSize: 14,
    color: '#666',
  },
  // Estilos para o modal
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  modalTitle: {
    marginBottom: 15,
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 20,
    color: "#3e945a"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 16,
  },
  // Estilos para o botão de voltar
  backButton: {
    position: "absolute",
    top: 40,
    left: 20,
    zIndex: 999,
  },
  // Estilos para o botão "Esqueceu a senha?"
  forgotPassword: {
    marginTop: 10,
  },
  forgotPasswordText: {
    color: "#3e945a",
    textDecorationLine: "underline",
    fontSize: 12,
  },
});

// Exportando o componente LoginScreen
export default LoginScreen;
