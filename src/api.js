import axios from "axios";

// Criação da instância do axios com configuração básica
const api = axios.create({
    baseURL: "http://10.89.234.178:5000/quadras", // URL base da API
    headers: {
        'Accept': 'application/json', // Cabeçalho padrão
    },
});

// Funções para interagir com a API
const apiFunctions = {
    // Quadras
    postQuadra: (quadra) => api.post("/quadrasPost/", quadra),
    updateQuadra: (id, quadra) => api.put(`/quadrasPut/${id}`, quadra),
    deleteQuadra: (id) => api.delete(`/quadrasDelete/${id}`),
    getQuadras: () => api.get("/quadrasGet/"),
    getQuadraById: (id) => api.get(`/quadrasGet/${id}`),

    // Usuários
    criarUsuario: (usuario) => api.post("/usuarioPost/", usuario),
    buscarUsuarios: () => api.get("/usuarioGet/"),
    getAllReservaByID: (id) => api.get(`/usuarioGet/${id}`),
    verificarReservaUsuario: (id) => api.get(`/usuarios/${id}`),
    postLogin: (usuario) => api.post("/postLogin", usuario),
    atualizarUsuario: (id, usuario) => api.put(`/AtualizarUsuario/${id}`, usuario),
    deletarUsuario: (id) => api.delete(`/DeletarUsuario/${id}`),

    // Reservas
    postReserva: (reserva) => api.post("/reservas", reserva),
    deleteReserva: (id) => api.delete(`/reservas/${id}`),
    updateReserva: (id, reserva) => api.put(`/reservas/${id}`, reserva),
    getAllReservaByID: (id) => api.get(`/reservas/${id}`),
    getAllReserva: () => api.get("/reservas"),
    getQuadrasPorUsuario: (id) => api.get(`/reservasdescription/${id}`),

    // DB
    getNameTables: () => api.get("/tables"),
    getTablesDescription: () => api.get("/tablesdescription"),
};

export default apiFunctions;
